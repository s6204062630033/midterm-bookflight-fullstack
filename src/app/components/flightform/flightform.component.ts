import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Flight } from './flight';

import { DisplaypageService } from 'src/app/modeldata/displaypage.service';

@Component({
  selector: 'app-flightform',
  templateUrl: './flightform.component.html',
  styleUrls: ['./flightform.component.css'],
})
export class FlightformComponent implements OnInit {
  flight!: Flight[];
  flightform: FormGroup;
  mindate!: Date;

  airportlocate = [
    {location : "Thai"},
    {location : "Japan"},
    {location : "UK"},
    {location : "USA"},
    {location : "Taiwan"}
  ];

  constructor(private fb: FormBuilder,private displaypage: DisplaypageService){
      this.flightform=fb.group({
        FormfullName: ['',[Validators.pattern('[a-zA-Z\s]{1,}'),Validators.required]],
        Formfrom: ['',[Validators.required]],
        Formto: ['',[Validators.required]],
        Formtype: ['',[Validators.required]],
        Formadults: ['',[Validators.required]],
        Formdeparture: ['',[Validators.required]],
        Formchildren: ['',[Validators.required]],
        Forminfants: ['',[Validators.required]],
        Formarrival: ['',[Validators.required]]
      })
      this.mindate = new Date();
   }

  ngOnInit(): void {
    this.getPages();
  }

  getPages() {
    this.flight = this.displaypage.getPages();
  }

  onsubmit(f: any){
    this.displaypage.addFlight(f);
    this.flightform.reset();
  }
}
