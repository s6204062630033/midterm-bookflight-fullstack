import { TestBed } from '@angular/core/testing';

import { DisplaypageService } from './displaypage.service';

describe('DisplaypageService', () => {
  let service: DisplaypageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DisplaypageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
