import { Flight } from "../components/flightform/flight";

export class Mockdata {
  public static mflight: Flight[]=[
    {
      fullName: "Nitchakarn Grabgeaw",
      from: "Thai",
      to: "UK",
      type: "One way",
      adults: 8,
      departure: new Date("2564-12-10"),
      children: 0,
      infants: 0,
      arrival: new Date("2564-12-12"),
    }
  ];
}
