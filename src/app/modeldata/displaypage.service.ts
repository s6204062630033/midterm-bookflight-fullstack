import { Injectable } from '@angular/core';
import { Flight } from '../components/flightform/flight';
import { Mockdata } from './mockdata';

@Injectable({
  providedIn: 'root'
})
export class DisplaypageService {
  flights: Flight[]=[];

  constructor() {
    this.flights = Mockdata.mflight;
  }

  getPages(){
    return this.flights;
  }

  addFlight(f:any){
    let typest="";
    if(f.Formtype==1){
      typest="One way";
    }
    else if(f.Formtype==2){
      typest="Return";
    }

    let dpt = new Date(f.Formdeparture);
    //console.log(dpt);
    //console.log(dpt.toLocaleDateString("th-TH"));
    let strdpt = (dpt.getFullYear()+543)+"-"+(dpt.getMonth()+1)+"-"+dpt.getDate();
    //console.log(strdpt);

    let arrv = new Date(f.Formarrival);
    let strarrv = (arrv.getFullYear()+543)+"-"+(arrv.getMonth()+1)+"-"+arrv.getDate();



    this.flights.push({
      fullName: f.FormfullName,
      from: f.Formfrom,
      to: f.Formto,
      type: typest,
      adults: f.Formadults,
      departure: new Date(strdpt),
      children: f.Formchildren,
      infants: f.Forminfants,
      arrival: new Date(strarrv),
    });
  }
}
